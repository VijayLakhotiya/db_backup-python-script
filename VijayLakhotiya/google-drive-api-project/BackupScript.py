#! Python
# from __future__ import print_function
import httplib2
import os, io
import auth

from googleapiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from apiclient.http import MediaFileUpload, MediaIoBaseDownload

import os
import time
import datetime         # Can delete it
import pipes

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import config

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None
    
# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/drive-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/drive'
CLIENT_SECRET_FILE = 'credentials.json'
APPLICATION_NAME = 'Quickstart'
authInst = auth.auth(SCOPES,CLIENT_SECRET_FILE,APPLICATION_NAME)
credentials = authInst.getCredentials()

http = credentials.authorize(httplib2.Http())
drive_service = discovery.build('drive', 'v3', http=http)
BACKUP_FOLDER_IN_DRIVE = config.BACKUP_FOLDER_IN_DRIVE
CONTAIN_MAX_BACKUP_IN_DRIVE = config.CONTAIN_MAX_BACKUP_IN_DRIVE

DB_HOST = config.DB_HOST
DB_USER = config.DB_USER
DB_USER_PASSWORD = config.DB_USER_PASSWORD
DB_NAMES = config.DB_NAMES           
BACKUP_PATH = config.LOCAL_BACKUP_PATH         

# Getting current DateTime to create the separate backup folder like "2018Oct17-12:34:33AM".
YEAR = time.strftime('%Y')
MONTH = time.strftime('%b')
DATETIME = time.strftime('%b-%d-%I:%M:%S%p')
TODAYBACKUPPATH = BACKUP_PATH + '/' + YEAR + '/' + MONTH + '/' + DATETIME

SENDER_MAIL = config.SENDER_MAIL_ID
MAIL_PASS = config.MAIL_PASSWORD

RECEIVER_MAIL = config.RECEIVER_MAIL_ID

print("SENDER_MAIL", SENDER_MAIL, "RECEIVER_MAIL", RECEIVER_MAIL)
                # uploadFile(DB_NAME+'.sql.gz',TODAYBACKUPPATH+'/' + DB_NAME + '.sql.gz','application/gzip')
def uploadFile(filename, folder_id, filepath, mimetype):
                # Start Creation of database backup to Google Drive
    file_metadata = {'name': filename, 'parents' : [folder_id] }
    media = MediaFileUpload(filepath, mimetype=mimetype)
    file = drive_service.files().create(body=file_metadata,
                media_body=media, fields='id').execute()
    # print('File ID: %s' % file.get('id'))

                # Start deletion of older database backup from Google Drive
    list_response = drive_service.files().list(
        orderBy   = "createdTime",
        q         = "name contains '"+DB_NAME+"' and name contains '.sql.gz'",
        pageSize  = 100,
        fields    = "files(id, name, createdTime)"
    ).execute()

    items = list_response.get('files', [])

    if items:
        print("Deletion started of DB ", DB_NAME,)
        for item in items[:-CONTAIN_MAX_BACKUP_IN_DRIVE]:
            print('Will try to delete this file:')
            print(u'{0} ({1}) {2}'.format(item['name'], item['id'], item['createdTime']))
            del_response = drive_service.files().delete(fileId=item['id']).execute()

    
def createFolder(name):
                # searchFile(size,query)
    query = "name contains '" + name + "'"
    results = drive_service.files().list(pageSize=10,
                fields="nextPageToken, files(id, name, createdTime, kind, mimeType)", q=query).execute()
    items = results.get('files', [])
    if not items:
        # print('No files found.')
        file_metadata = {
        'name': name,
        'mimeType': 'application/vnd.google-apps.folder'
        }
        file = drive_service.files().create(body=file_metadata,
                                            fields='id').execute()
        # print("in createing:", file.get('id'))
        return file.get('id')

    else:
        # print('\n{0} ({1})'.format(items[0]['name'], items[0]['id']))
        return items[0]['id']

        # Checking if backup folder already exists or not. If not exists will create it.
try:
    os.stat(TODAYBACKUPPATH)
except:
    try:
        os.stat(BACKUP_PATH + '/' + YEAR + '/' + MONTH )
        os.mkdir(TODAYBACKUPPATH)
    except:
        try:
            os.stat(BACKUP_PATH + '/' + YEAR )
            os.mkdir(BACKUP_PATH + '/' + YEAR + '/' + MONTH )
            os.mkdir(TODAYBACKUPPATH)
        except:
            os.mkdir(BACKUP_PATH + '/' + YEAR)
            os.mkdir(BACKUP_PATH + '/' + YEAR + '/' + MONTH )
            os.mkdir(TODAYBACKUPPATH)
drive_folder_id = createFolder(BACKUP_FOLDER_IN_DRIVE)
# print("i am drive_folder_id", drive_folder_id)

mail_content = ""
S_No = 1
status = "Backup Failed"
for DB_NAME in DB_NAMES:
    mail_content += "<tr> <td>"+str(S_No)+" <td>"+ DB_NAME +" </td>"
    # print ("Starting backup of database " + DB_NAME)    
    try:
        var = os.system("mysql -u "+ DB_USER + " -p" + DB_USER_PASSWORD + " -e " + "'USE "+ DB_NAME + "'")
        f = open(BACKUP_PATH+"/cFile.txt", "a+")
        f.write("Backup of " + DB_NAME + " Successfully completed on " + DATETIME + " \n\n")
        # print("Hey, You are giving A wrong Database Name.")
        f.close()
        
    except Exception as err:
        f = open(BACKUP_PATH+"/cFile.txt", "a+")
        f.write("Backup of " + DB_NAME + " Failed on " + DATETIME + " \n Error:\t" + format(err) +" \n\n")
        f.close()

    File_Exists = os.path.exists('cFile.txt')
    try:
        dumpcmd = "mysqldump -h " + DB_HOST + " -u " + DB_USER + " -p" + DB_USER_PASSWORD + " " + DB_NAME + " > " + pipes.quote(TODAYBACKUPPATH) + "/" + DB_NAME + ".sql"
        status = "Backup Successfully Created"
    except:
        status = "Backup Failed"

    os.system(dumpcmd)
    gzipcmd = "gzip " + pipes.quote(TODAYBACKUPPATH) + "/" + DB_NAME + ".sql"
    os.system(gzipcmd)
    mail_content += " <td>"+ status +" </td> </tr>"
    uploadFile(DB_NAME+"_"+DATETIME+'.sql.gz', drive_folder_id, TODAYBACKUPPATH+'/' + DB_NAME + '.sql.gz', 'application/gzip')
    S_No += 1
# print ("Your backups have been created in '" + TODAYBACKUPPATH + "' directory")

            # Email the DB-Backup-Report
s = smtplib.SMTP('smtp.gmail.com', 587) 
  
s.starttls()        # start TLS for security 

s.login(SENDER_MAIL, MAIL_PASS)          # Authentication 

msg = MIMEMultipart('alternative')

text = "Hi!\nHow are you?\nHere is the link you wanted:\nhttp://www.python.org"
html = """\
<html>
    <head>
        <style>
            th {
                font-size: 16px;
            }
            .first_div {
                font-size: 14px;
                text-align: left;
            }
            .p_text {
                font-size: 12px;
            }
        </style>
    </head>
    <body>
        <p class="p_text">Hi,<br>
            your routine backup for OTOO TUITIONS is captured on """+DATETIME+"""<br>
            You Can check it in Drive
        </p>
        <div class="first_div">
            <table Cellpadding = '8' Cellspacing = '12'>
                <tr>
                    <th>S.No.</th>
                    <th>DB Name</th>
                    <th>Status</th>
                </tr>
                """+mail_content+"""
            </table>
        </div>
    </body>
</html>
"""

part2 = MIMEText(html, 'html')
msg.attach(part2)
msg['Subject'] = "Database "+status
s.sendmail(SENDER_MAIL, RECEIVER_MAIL, msg.as_string())           # sending the mail

s.quit()            # terminating the session 
# print("Mailing Successfully Completed")
