# Python-script for Database-Backup

## Requirements
Python version 3.*
Pip # (any)
<!-- Text-Editor -->

## Installation

First, please create and Activate your Virtualenvironment
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install

Clone or Download this project and unzip.
change directory to the req.txt file
```bash
pip install -r req.txt
```

## Usage
To use this python script please Change in the config.py file.

If you want to make backup manually then Open your terminal and run the command as below(To create backup and Upload it in a folder of Google Drive):

```bash
python BackupScript.py
or
sudo python BackupScript.py
```

Set cron:
```bash
crontab -e
*/3 * * * * /usr/bin/python3 /home/auriga/Desktop/pythonscript/google-drive-api-tutorial-master/google-drive-api-project/BackupScript.py 2>> /home/auriga/logfile.txt


```

